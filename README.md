# There are other timelines
[ Download the application ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/there-are-other-timelines-release.zip)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/DeLorean%20-%20BTTF.png)

## Sections
* [Basic information](#markdown-header-basic-information)  
* [Disclaimer](#markdown-header-disclaimer)
* [Version history](#markdown-header-version-history)
* [Future versions](#markdown-header-future-versions)
* [How to use](#markdown-header-how-to-use)
* [Examples](#markdown-header-examples)
	* [Back to the Future Part I](#markdown-header-back-to-the-future-part-i)
	* [DeLorean in Back to the Future trilogy](#markdown-header-delorean-in-back-to-the-future-trilogy)
	* [Frequently Asked Questions about Time Travel](#markdown-header-frequently-asked-questions-about-time-travel)
	* [Futurama: The Lost Adventure](#markdown-header-futurama-the-lost-adventure)
	* [Tenet](#markdown-header-tenet)
	* [Oscar](#markdown-header-oscar)

## Basic information
"There are other timelines" is an application primarily intended for visualizing events in time travel movies, movies containing travel between alternate timelines, or even a non-sci-fi fiction, like alibi of characters in a detective story, or possession of items in a farce play.
If you get the reference in the title of the application, you're streets ahead! ;)
The application was developed as a Visualization class project using the C# programming language and Windows Presentation Foundation (in Visual Studio), and as such, is only guaranteed to work in a Windows Operating System (Vista or newer). 
There might be some ways to make it work on other systems, too, I haven't much looked into it.

I decided to make this project, because I have always been drawn to time travel films and TV shows, sometimes not sure of sequence of some details and often considering how logically is the time travel in the specific work of fiction constructed. 
This program is intended to use by me and other like-minded geeks who like to overanalyze films ;)

If you're reading the offline version of this documentation, the link to the official release repository is [ https://bitbucket.org/Zelnidav/there-are-other-timelines-release ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release)

## Disclaimer
As the program is a one-man project, developed during a few weeks, be prepared for the potential of some bugs or crashes and better save your projects often. I think I took care of the biggest issues, but every example project that I did so far showed me some room for improvement (which I then took), but it's possible that I would find even more shortcomings had I made more example projects. 
In case your project doesn't save properly, be aware that a .taot file is actually just a .zip file with different extension, containing an .xml project file (containg first and foremost the command history) and files from the associated image library.

## Version history
**v1.1**

- Hierarchial structure in the saved project file (faster loading, but also backwards compatibility)
- Checkbox to either Show only visited events (as before) or all events (more user-friendly)
- Two new commands - World Timeline Merge for simplifying a chart, and Entity Concat for reveals of two entities being the same one
- Is jinn - fixed loading
- Removed dll dependencies, meaning the released exe can work on its own (at least in the current version)

**v1.01**

- Fixed the title of change shown title window, fixed the typo in the exe name
- Included version number property in the application
- Better support for updating release version

**v1.0**

- Initial release  

## Planned features
Since this is a hobby one-man project, I cannot guarantee what the future of this project will be, or set up any deadlines. However, these are the currently planned future versions:

- **v1.2** - Zoom, drag & drop in list-like editor panels on the left
- **v1.3** - Hover and selection outline, more direct controls inside the main editor panel on the right, influencing the order on bars/rows inside an individual timeline
- **v2.0 and further** - Exporting an animation - showing and hiding an object from a keyframe, zooming on an object at a keyframe. Marking additional notes and associations between events.                                                                     

## How to use
The left portion of the window is used for inputing world timelines and entities. 
The right portion(**1**) shows the output and is not edittable (no drag-and-drop adjustments). 
This is a bit rigid, but I feel like offering more freedom to the user might be contra-productive, as the program's main feature is automated image generation from quite simple data structure. 

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/tutorial-images/tutorial1.png)

The timelines panel(**2**) can be used to create timelines with unique identifier names, which are visualized as rectangles. 
Each timeline can be further edited(**3**) (its name and color) by clicking on the pencil button and returning anytime with the "go back" button, and the timeline holds a set of timeline events, each with a unique identifier moment (DateTime), and optional label and image. 
The moments are only used for internal purposes (spacing between timeline events) and concrete dates and times won't show up in the final images as text, so you don't have to be completely exact. 
I apologize for the clunkiness of the time portion of the moments, but that is the pre-made component which I only used and re-making the component would be a little too tedious. 
The spacing is based on logarithmic scale, making shorter durations between events appear as closer spacing and vice versa, as linear scale would contain overlaps (for example, in Back to the Future, one week compared to 30 years would be almost invisible on a linear scale), and uniform scale would remove any sense of proportionality.
The direction from left to right signifies moving from past to future, and vice versa.  

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/tutorial-images/tutorial2.png)

Similarly you can create entities(**4**) with unique identifier names, editable color (which actually produces color gradient with darker shades at the beginning of the entity trajectory and darker by the end) and jinn property (meaning circular trajectory through time). 
Entities can represent characters or objects moving through time. Each entity holds a list of entity events(**5**) (again accessed by the pencil button), with each entity event associated with one of previously mentioned timeline events. 
Only those timelines, timeline events and entities, which were used by at least one entity event, will appear in the result, so don't get discouraged if you for example don't see a timeline that hasn't been travelled to in the exported image. 
Entity events are automatically positioned and connected by arrows in sequence from the point-of-view of the entity. 
If an arrow leaves a timeline rectangle, this signifies a jump in time (or between timelines). The jumps to past are detected automatically, jumps to future are marked by the "enforce jump to" checkbox to differentiate between moving between events in real time and jumping to future. 
Entity events, similarly to timeline events, can have an associated label and image. Also, the sequence of timelines, entities, and entity events can be modified with buttons with up and down arrows. Timeline events are sorted automatically by their identifier moments. 

At the top of the shown result, there is a toolbar(**6**) where you can change a shown title or background image of the whole diagram. On the right of this toolbar, you can access the image library of the project. Only images loaded in the image library can be used by events and as a background image. The image library and all other controls in this toolbar are independent on command history (meaning undo won't unload images in the image library). 
Also, there is an "enable inversion" checkbox, allowing an arrow from right to left which doesn't leave the timeline rectangle, meaning moving back in time, but in real time. This was pretty much only made for Tenet (but I would also use it for Primer and can imagine there's some other use for this functionality).

The top-left toolbar(**7**) is pretty standard - New project (Ctrl+N), Open project (Ctrl+O), Undo (Ctrl+Z), Redo (Ctrl+Y), Save project (Ctrl+S), Export project as image, Export project as video (disabled, this feature might appear in a potential future version of the program), Info window (F1). 

If you still don't understand how to operate this program, I suggest you open some of the example projects and try to play around with them (you can kind of see how they were gradually made by viewing the command history - the undo and redo buttons).   

## Examples
### Back to the Future Part I
[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/BTTF%20I.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/BTTF%20I.png)
### DeLorean in Back to the Future trilogy
[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/DeLorean%20-%20BTTF.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/DeLorean%20-%20BTTF.png)
### Frequently Asked Questions about Time Travel
The full version:

[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/FAQaTT.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/FAQaTT.png)

The simplified version:

[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/FAQaTT%20simplified.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/FAQaTT%20simplified.png)
### Futurama: The Lost Adventure
I chose this game to showcase the jinn feature (jinn is an entity which has a circular trajectory through time, meaning it never started or stopped existing, and at some point became an earlier version of itself).

[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Futurama%20-%20The%20Lost%20Adventure.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Futurama%20-%20The%20Lost%20Adventure.png)
### Tenet
I made three alternative versions, not knowing which one is the most useful.

[ Project link 1 ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet.taot)

[ Project link 2 ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet%20simplified.taot)

[ Project link 3 ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet%20simplified%201timeline.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet.png)
![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet%20simplified.png)
![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Tenet%20simplified%201timeline.png)
### Oscar
Finally, I made an example to showcase that this program can also be used for films that aren't science fiction. 
This diagram shows the possession of three different suitcases by different characters in the film Oscar.

[ Project link ](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Oscar.taot)

![Alt text](https://bitbucket.org/Zelnidav/there-are-other-timelines-release/raw/master/example%20projects%20and%20exports/Oscar.png)  
